import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../data.service';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {

  constructor(private route: ActivatedRoute, private location: Location, private dataService: DataService, private spinner: NgxSpinnerService) { }

  searchResultList: Object;
  searchStoreResultList: Object;
  searchQueryParam: string;
  couponSelected: Object;
  
  ngOnInit() {
    this.getSearchResult();
  }

  getSearchResult():void {
    this.route.queryParams.subscribe(queryParams => {
      // do something with the query params
    });
    this.route.params.subscribe(routeParams => {
      this.searchQueryParam = routeParams.q;
      this.fetchResult(routeParams.q);
    });    
  }

  fetchResult(q): void {
    this.spinner.show();
    this.dataService.getSearchResult(q)
      .subscribe(data => {
        //console.log("Search Result:", data);
        this.searchResultList = data["results"]; 
        // this.categoryName = this.dataService.selectedCategory;       
        // //console.log(this.categoryName);
        this.spinner.hide();
    });
    this.dataService.getStoreSearchResult(q)
      .subscribe(data => {
        //console.log("Search Result:", data);
        this.searchStoreResultList = data; 
        // this.categoryName = this.dataService.selectedCategory;       
        // //console.log(this.categoryName);
        this.spinner.hide();
    });
  }

  onCouponSelected(list): void{
    //console.log(list);
    this.couponSelected = list;
    var goto_link = this.couponSelected["goto_link"];
    setTimeout(function(){      
      //console.log("URL", goto_link)
      window.open(goto_link, "_blank");
    }, 3000)
  }

  copyCouponTxt(): void{
    var copyText = document.getElementById("promocode") as HTMLInputElement;
    copyText.select();
    document.execCommand("copy");
    var goto_link = this.couponSelected["goto_link"];
    setTimeout(function(){      
      //console.log("URL", goto_link)
      window.open(goto_link, "_blank");
    }, 1000)
  }

}
